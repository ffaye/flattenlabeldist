# FlattenLabelDist
*A robust algorithm for extending samples with rare labels in multi-label datasets.*

Have you ever worked with a multi-label dataset\* and found that some labels 
were very overrepresented, while others were very rare? If so, you may have wanted
to extend samples with rare labels (e.g. through data-augmentation), to even 
out the distribution a bit. However, what if some samples with rare labels also 
had frequent labels? Should you extend these samples or not? Which samples should 
be extended the most, so as to achieve the most even distribution of labels?

This algorithm (`flattenlabeldist.py`) takes a dataset and extends samples with 
rare labels, so as to make the label distribution associated with the samples more uniform.
Specifically, it takes the so-called label matrix\** of a multi-label dataset and the 
number of wanted extensions, and returns the IDs of the samples that should be 
extended, as well as how many times each sample should be extended. 

\* A dataset is called *multi-label* if each sample can have multiple labels. Object
recognition datasets are often multi-label, in that each image has a list of 
labels (objects) associated with it.

\** The *label matrix* is an *N* times *K* matrix *Y* with *Y_ij = 1* if sample *i* 
has label *j* and *Y_ij = 0* otherwise, where *N* is the number of samples in 
the dataset and *K* is the number of different possible labels.

![](images/example.png)

The above figure is an illustration of what the algorithm does: It takes some 
dataset, finds samples whose labels are underrepresented and extends them to 
make the distribution of labels more uniform.

### Table of Content
- [Usage](#usage)
- [Documentation](#documentation)
    - [Args](#args)
    - [Attributes](#attributes)
- [Todo](#todo)

<a name="usage"></a>
## Usage 
No installation is required. Simply place `flattenlabeldist.py` in the directory 
in which you want to use it and include `from flattenlabeldist import FlattenLabelDist` 
in your script. Instantiate the class and call the `run()` 
method in order to carry out the wanted extensions.

Please see the `Tutorial.ipynb` for more details, both on how to use the class
and on how the algorithm works.

Note that `Python3` and the progress-bar module `tqdm` are required (although the 
class could easily be rewritten for `Python2.7` if needed). 

<a name="documentation"></a>
## Documentation 

```python
class FlattenLabelDist:
    """Extends samples with rare labels to even out the distribution of labels.

    This class extends samples with rare labels, so as to even out the label
    distribution associated with the samples.
    This is especially useful in the context of doing data augmentation in
    multi-label classification problems.
    Note that method ``run()`` must be called for the wanted extensions to be
    carried out.
    """
    
    def __init__(self, labelMatrix, nExtensions=None, nMaxFails=1000, 
                 nLogs=1000, score='entropy', verbose=True)
```

<a name="args"></a>
#### Args : 
- **labelMatrix** : *array* 

    2D array with a 1 at index [i,j] if sample i has label j, and
    0 otherwise.

- **nExtensions** : *int, optional (default=None)*

    The number of wanted extension.
    When None, `nExtensions` will be set to be equal to the number of samples 
    represented in labelMatrix.
    
- **nMaxFails** : *int, optional (default=1000)*

    The maximum number of allowed consecutive times
    a label that did not result in an extension of any sample with that
    label can be chosen without the script terminating. This is to
    prevent the script from not terminating.
    
- **nLogs** : *int, optional (default=1000)*

    The number of points collected for attributes `successRatios` and 
    `percentPosImp`. If `nExtensions`<`nLogs`, `nLogs` will be set to
    `nExtensions`.
    
- **score** : *string, optional (default='entropy')*

    Determines which scoring method is used to assess whether or not
    a given extension of a sample leads to a more uniform distribution.
    Must be either 'entropy', 'MAE' (mean absolute error), or 'MSE'
    (mean squared error).
    
- **verbose** : *int, optional (default=True)*

    Verbose outputs. The default (True) results in a `tqdm` progress 
    bar.

<a name="attributes"></a>
#### Attributes :
- **run()** : 

    Method carrying out the wanted extension.

    **Returns** :
    
    - **extensionIDs** : *array*
    
        1D array with integer value *v* in its ith place if sample
        i has been extended *v* times,
        
    - **labelDist** : *array*
    
        1D array containing the resulting (not normalized)
        distribution of labels, with integer value *v* in its jth
        place if label j is represented *v* times in the dataset
        subsequent to extension.

- **successRatios** : *array, shape=(`nLogs`, 2)*

    2D array collecting in its second column the number of
    successful extensions divided by the number of attempted extensions
    after *n* succesful extensions has been carried out, where *n* is
    the value in the first column.
- **percentPosImp** : *array, shape=(`nLogs`, 2)*

    2D array collecting in its second column the number of
    labels which have come closer to uniformity divided by the total
    number of labels after *n* succesful extensions has been carried out,
    where *n* is the value in the first column.
    
<a name="todo"></a>
## Todo 
* Make stopping criterion that triggers if number of iterations per second dips under some preset value. 
