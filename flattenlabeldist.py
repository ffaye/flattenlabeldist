import numpy as np
from tqdm import tqdm

class FlattenLabelDist:
    """Extends samples with rare labels to even out the distribution of labels.

    This class extends samples with rare labels, so as to even out the label
    distribution associated with the samples.
    This is especially useful in the context of doing data augmentation in
    multi-label classification problems.
    Note that method `run()` must be called for the wanted extensions to be
    carried out.

    Args :
        labelMatrix :
            *array*

            2D array with a 1 at index [i,j] if sample i has label j, and
            0 otherwise.
        nExtensions :
            *int, optional (default=None)*

            The number of wanted extension.
            When None, `nExtensions` will be set to be equal to the number of
            samples represented in labelMatrix.
        nMaxFails :
            *int, optional (default=1000)*

            The maximum number of allowed consecutive times
            a label that did not result in an extension of any sample with that
            label can be chosen without the script terminating. This is to
            prevent the script from not terminating.
        nLogs :
            *int, optional (default=1000)*

            The number of points collected for attributes `successRatios` and
            `percentPosImp`. If `nExtensions`<`nLogs`, `nLogs` will be set to
            `nExtensions`.
        score :
            *string, optional (default='entropy')*

            Determines which scoring method is used to assess whether or not
            a given extension of a sample leads to a more uniform distribution.
            Must be either 'entropy', 'MAE' (mean absolute error), or 'MSE'
            (mean squared error).
        verbose :
            *int, optional (default=True)*

            Verbose outputs. The default ('True') results in a `tqdm` progress
            bar.

    Attributes :
        run() :
            Method carrying out the wanted extension.

            Returns :
                extensionIDs :
                    *array*

                    1D array with integer value *v* in its ith place if sample
                    i has been extended *v* times,
                labelDist :
                    *array*

                    1D array containing the resulting (not normalized)
                    distribution of labels, with integer value *v* in its jth
                    place if label j is represented *v* times in the dataset
                    subsequent to extension.
        successRatios :
            *array, shape=(`nLogs`, 2)*

            2D array collecting in its second column the number of
            successful extensions divided by the number of attempted extensions
            after *n* succesful extensions has been carried out, where *n* is
            the value in the first column.
        percentPosImp :
            *array, shape=(`nLogs`, 2)*

            2D array collecting in its second column the number of
            labels which have come closer to uniformity divided by the total
            number of labels after *n* succesful extensions has been carried out,
            where *n* is the value in the first column.
    """

    def __init__(self, labelMatrix, nExtensions=None, nMaxFails=1000,
                 nLogs=1000, score='entropy', verbose=True):
        self.labelMatrix = np.array(labelMatrix)
        self._nLabels = self.labelMatrix.shape[1]
        self.extensionIDs = np.zeros(self.labelMatrix.shape[0]).astype(int)
        self._initialLabelDist = np.sum(self.labelMatrix, axis=0).astype(int)
        self.labelDist = self._initialLabelDist
        self._verbose = verbose
        if nExtensions == None:
            self.nExtensions = self.labelMatrix.shape[0]
            if self._verbose:
                tqdm.write('nExtensions set to labelMatrix.shape[0].')
        else:
            self.nExtensions = nExtensions
        self.nMaxFails = nMaxFails
        self._nAttemptedExtensions = 0
        self._nSuccessfulExtensions = 0
        self._nFails = 0
        self._lastFailedLabel = -1
        self.score = score
        if score not in ['entropy','MAE','MSE']:
            tqdm.write('ERROR: {} is not a valid score.'.format(score))
            quit()
        if (self.score == 'entropy') and np.any(np.sum(self.labelMatrix, axis=0) < 1):
            tqdm.write('ERROR: When using entropy as score, each label must be represented at least once.')
            quit()
        if self._verbose:
            self._pbar = tqdm(total=self.nExtensions)
        self._currentScore = self._calcScore(self.labelDist)
        self._indsWithLabel = [np.argwhere(self.labelMatrix[:,label]==1).flatten() for label in range(self._nLabels)]
        self.successRatios = []
        self.percentPosImp = []
        if self.nExtensions < nLogs:
            self.nLogs = self.nExtensions
        else:
            self.nLogs = nLogs

    def _calcScore(self, labelDist):
        p_j = 1.0*labelDist/np.sum(labelDist, axis=0)
        if self.score == 'entropy':
            S = -np.sum(p_j*np.log(p_j), axis=0)
            return 1-S/np.log(self._nLabels)
        elif self.score == 'MAE':
            return np.sum(np.abs(p_j - 1/self._nLabels), axis=0)/self._nLabels
        elif self.score == 'MSE':
            return np.sum((p_j - 1/self._nLabels)**2, axis=0)/self._nLabels

    def _getLabel(self, labelDist):
        addedExtremes = np.max(labelDist) + np.min(labelDist)
        labelDistReflected = (1-labelDist/addedExtremes)/(self._nLabels-np.sum(labelDist)/addedExtremes)
        return next(j for j,m in enumerate(np.cumsum(labelDistReflected)) if m > np.random.rand())

    def _extend(self, label):
        # Shuffle indices (reflect and translate) of samples with the drawn label
        if np.random.randint(2):
            self._indsWithLabel[label] = np.flipud(self._indsWithLabel[label])
        shuffledIndices = np.roll(self._indsWithLabel[label],np.random.randint(self._initialLabelDist[label]))
        for index in shuffledIndices:
            # Try extension:
            extendedSample = self.labelMatrix[index,:]
            extendedLabelDist = self.labelDist + extendedSample
            extendedScore = self._calcScore(extendedLabelDist)
            self._nAttemptedExtensions += 1
            # Accept extension if score is lowered:
            if extendedScore < self._currentScore:
                self.extensionIDs[index] += 1
                self.labelDist = extendedLabelDist
                self._currentScore = extendedScore
                self._nFails = 0
                self._lastFailedLabel = -1
                self._nSuccessfulExtensions += 1
                if (self._nSuccessfulExtensions%np.round(self.nExtensions/self.nLogs) == 0):
                    self.successRatios.append([self._nSuccessfulExtensions, self._nSuccessfulExtensions/self._nAttemptedExtensions])
                    initLabelDist = self._initialLabelDist/np.sum(self._initialLabelDist)
                    extLabelDist = self.labelDist/np.sum(self.labelDist)
                    uniformLabelDist = 1/self._nLabels
                    improvement = np.sign((uniformLabelDist-initLabelDist)*(extLabelDist-initLabelDist))*np.abs(initLabelDist-extLabelDist)
                    uniformInbetween = ((initLabelDist < uniformLabelDist)*(uniformLabelDist < extLabelDist)
                                        + (extLabelDist < uniformLabelDist)*(uniformLabelDist < initLabelDist))
                    improvement[uniformInbetween] -= uniformLabelDist-np.minimum(initLabelDist[uniformInbetween],extLabelDist[uniformInbetween])
                    self.percentPosImp.append([self._nSuccessfulExtensions, np.sum(improvement > 0)/self._nLabels])
                if self._verbose:
                    self._pbar.update(1)
                return
        self._nFails += 1 # Happens only if no extension was accepted during the above for loop.
        self._lastFailedLabel = label

    def run(self):
        while ((self._nSuccessfulExtensions < self.nExtensions) and (self._nFails < self.nMaxFails)):
            labelToBeExtended = self._getLabel(self.labelDist)
            while labelToBeExtended == self._lastFailedLabel:
                self._nFails += 1
                labelToBeExtended = self._getLabel(self.labelDist)
            self._extend(labelToBeExtended)
            if not (self._nFails < self.nMaxFails):
                tqdm.write('''Extending all samples with {} consecutively chosen labels did not lead to a more uniform distribution:
                Further extensions aborted.
                To increase the maximum acceptable number of consecutive fails, change nMaxFails.
                {} extensions were made prior to abort.'''.format(self._nFails, self._nSuccessfulExtensions))
        self.successRatios = np.array(self.successRatios)
        self.percentPosImp = np.array(self.percentPosImp)
        return self.extensionIDs, self.labelDist
